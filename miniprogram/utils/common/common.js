"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Resource ={
  getResource(list, method,data){
    return new Promise((resolve, reject) => {
        wx.request({
          url: 'http://localhost:3001/' + list,
          method: method,
          header: {
            'content-type': 'application/json', // 默认值
          },
          data:data,
          success(res) {
            console.log(res);
            resolve(res);
          },
          fail(res){
            reject(res);
          }
        })
    });
  }
}
exports.Resource = Resource;