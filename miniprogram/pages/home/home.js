// miniprogram/pages/home/home.js
var app = getApp();
var resource = require('../../utils/common/common.js');
var timer;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    softwareList: [],
    inputValue:'',
    page:1,
    limit:5,
    loading:'hidden',
    nodata:'hidden'
  },
  navigateToDetails:function(e){
    let _id = e.currentTarget.id
    wx.navigateTo({
      url: '../../pages/details/details?id='+_id
    })
  },
  bindKeyInput(e) {
    let value = e.detail.value;
    if (e.detail.value != '') {
      app.getResource('homePageList?softName_like=' + value, 'get').then((res) => {
        this.setData({
          softwareList: res.data
        })
      });
    }else{
      app.getResource('homePageList', 'get').then((res) => {
        this.setData({
          softwareList: res.data
        })
      });
    }
  },
  blur() {
    this.setData({
      inputValue: ''
    })
    app.getResource('homePageList', 'get').then((res) => {
      this.setData({
        softwareList: res.data
      })
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    app.getResource('homePageList?_page=' + this.data.page + '&_limit=' + this.data.limit, 'get').then((res) => {
      this.setData({
        softwareList: res.data
      })
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.pageScrollTo({
      scrollTop: 0,
      duration: 300
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function (e) {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function (e) {
    clearTimeout(timer);
    this.data.loading=''
    var _page = this.data.page + 1
    this.setData({
      page: _page,
      loading:'',
      nodata: 'hidden'
    })
    var that=this
    app.getResource('homePageList?_page=' + _page + '&_limit=' + this.data.limit, 'get').then((res) => {
      timer = setTimeout(function () {
        console.log(res.data)
        if (res.data && res.data.length>0){
          that.setData({
            softwareList: that.data.softwareList.concat(res.data),
            loading: 'hidden',
            nodata: 'hidden'
          })
        }else{
          that.setData({
            loading: 'hidden',
            nodata: ''
          })
        }
        
      }, 1000);
       
    });
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})