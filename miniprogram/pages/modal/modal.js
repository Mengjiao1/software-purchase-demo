var app = getApp();
var resource = require('../../utils/common/common.js');
Component({
  properties: {
    modalId: String // 简化的定义方式
  },
  data: {
    animationData: '',
    showModalStatus: false,
    softwareDetails: '',
    defaultType: 0,
    totalPrice:'',
    buyNum:1,
  },
  methods: {
    // 这里放置自定义方法
    showModal: function () {
      // 显示遮罩层
      var animation = wx.createAnimation({
        duration: 200,
        timingFunction: "linear",
        delay: 0
      })
      this.animation = animation
      animation.translateY(300).step()
      this.setData({
        animationData: animation.export(),
        showModalStatus: true
      })
      setTimeout(function () {
        animation.translateY(0).step()
        this.setData({
          animationData: animation.export()
        })
      }.bind(this), 200)
      app.getResource('homePageList/' + this.data.modalId, 'get').then((res) => {
        this.setData({
          softwareDetails: res.data,
          totalPrice: res.data.price,
          defaultType:0,
          buyNum: 1
        })
      });
    },
    hideModal: function () {
      // 隐藏遮罩层
      var animation = wx.createAnimation({
        duration: 200,
        timingFunction: "linear",
        delay: 0
      })
      this.animation = animation
      animation.translateY(300).step()
      this.setData({
        animationData: animation.export(),
      })
      setTimeout(function () {
        animation.translateY(0).step()
        this.setData({
          animationData: animation.export(),
          showModalStatus: false
        })
      }.bind(this), 200)
      this.triggerEvent("hideModalEvent"); 
      this.setData({
        softwareDetails: '',
        totalPrice: ''
      })
    },
    _selectType: function (e) {

      this.setData({
        defaultType: e.currentTarget.dataset.type,
        totalPrice: e.currentTarget.dataset.totalprice,
      })
    },
    _addToShopCart: function () {
      var optionId = this.data.modalId
      app.getResource('homePageList/' + optionId, 'get').then((res) => {
        var thisData = res.data;
        thisData.classType = this.data.defaultType;
        thisData.totalPrice = this.data.totalPrice * this.data.buyNum;
        thisData.buyNumber = this.data.buyNum;

        app.getResource('purchaseList/' + optionId, 'get').then((res) => {
          if (res.statusCode == 200 && res.data) {
            app.getResource('purchaseList/' + optionId, 'put', thisData).then((res) => {
              this.hideModal();
            });
          } else {
            app.getResource('purchaseList', 'post', thisData).then((res) => {
              this.hideModal();
            });
          }
        });

      })

    },
    //检查输入数量
    checkNum: function (res) {
      var that = this
      var num = res.detail.value
      if (num <= 0 || num >= 6 || num == "") {
        wx.showToast({
          title: '输入超出范围',
          icon: 'none'
        })
        that.setData({
          buyNum: 1
        })
      }
    },
    //获取输入数量
    getInputNum: function (res) {
      var that = this
      var num = res.detail.value
      that.setData({
        buyNum: parseInt(num)
      })
    },
    //计算数量
    countNum: function (res) {
      var that = this
      var type = res.currentTarget.dataset.type
      var num = that.data.buyNum
      if (type == 1) {
        if (num > 1) {
          that.setData({
            buyNum: parseInt(num - 1)
          })
        }
      }
      if (type == 2) {
        if (num < 5) {
          that.setData({
            buyNum: parseInt(num + 1)
          })
        }
      }
    },

  }
})