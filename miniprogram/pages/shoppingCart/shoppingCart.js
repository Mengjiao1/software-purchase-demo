// miniprogram/pages/shoppingCart/shoppingCart.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    shoppingList:[],
    totalNum:0,
    totalPrice:0,
    checkboxTrue:false,
    checkAllTrue:false,
    checkList:[],
    modalId:'',
    animationData: '',
    showModalStatus: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },
  getShoppingList: function (e) {
    app.getResource('purchaseList', 'get').then((res) => {
      this.setData({
        shoppingList: res.data
      })
      wx.setStorage({
        key: "allkeyLength",
        data: res.data.length
      })
    });
  },
  chooseCheck: function () {
    if (this.data.checkAllTrue) {
      this.data.shoppingList.forEach((item, idx, self) => {
        item.checked = false;
      });
      this.setData({
        checkboxTrue: false,
        checkAllTrue: false,
        totalNum: 0,
        totalPrice: 0,
        checkList: [],
        shoppingList: this.data.shoppingList,
      });
    }else{
      let price = 0;
      let num = 0;
      this.data.shoppingList.forEach((item, idx, self) => {
        price += parseInt(item.totalPrice * 100);
        num += item.buyNumber;
        item.checked = true;
      });
      this.setData({
        checkboxTrue: true,
        checkAllTrue: true,
        totalNum: num,
        totalPrice: price / 100,
        checkList: this.data.shoppingList,
        shoppingList: this.data.shoppingList
      });
    }
  },
  // chooseCheck: function () {
  //   if (this.data.checkAllTrue) {
  //     this.setData({
  //       checkboxTrue:false,
  //       checkAllTrue:false,
  //       totalNum:0,
  //       totalPrice:0,
  //       checkList:[]
  //     });
  //     this.data.shoppingList.forEach((item,idx,self)=>{
  //       item.checked = false;
  //     });
  //   }else {
  //     let price = 0;
  //     let num = 0;
  //     this.data.shoppingList.forEach((item,idx,self)=>{
  //       price += parseInt(item.totalPrice*100);
  //       num += item.buyNumber;
  //       item.checked = true;
  //     });
  //     let dataList = this.data.shoppingList;
  //     this.setData({
  //       checkboxTrue: true,
  //       checkAllTrue: true,
  //       totalNum: num,
  //       totalPrice: price/100,
  //       checkList: dataList
  //     });
  //   }
  // },
  deleteShopping : function (e) {
    app.getResource('purchaseList/'+e.currentTarget.dataset.id,'delete').then((res)=>{
      app.getResource('purchaseList','get').then((response)=>{
        this.setData({
          shoppingList:response.data
        })
      })
    })
  },
  editShopping: function (e) {
    this.setData({
      modalId: e.currentTarget.dataset.id
    })
    this.selectModal.showModal();

  },
  _hideModalEvent() {
    this.getShoppingList();
  },
  settleTotal: function() {
    let dataNo = 0; 
    this.data.checkList.forEach((item,idx,self)=>{
      dataNo = String(new Date().getTime()) + String(item.id);;
      item.orderNo = dataNo;
      item.orderId = item.id;
      item.id = dataNo;
      item.timeStamp = new Date().getTime();
      item.orderType = '订单已完成';
    });
    this.data.checkList.forEach((item,idx,self)=>{
    
      app.getResource('personalList', 'post', item).then((res) => {
       
        app.getResource('purchaseList/' + item.orderId,'delete').then((res)=>{
          app.getResource('purchaseList', 'get').then((response) => {
            if(res.statusCode == 200 && res.data){
              this.setData({
                checkList: [],
                shoppingList: response.data,
                totalNum: this.data.totalNum,
                totalPrice: this.data.totalPrice
              });
              wx.showToast({
                icon: 'none',//提示图标
                title: 'Purchase Success！',
                duration: 2000//提示的时间毫秒
              })
            }
          });
        })
      })
    })
  },
  checkboxChoose: function (e) {
    let target = e.currentTarget.dataset;
   
    let nowPrice = parseInt(this.data.totalPrice * 100);
    let checklist = this.data.checkList;
    let nowNum = this.data.totalNum;
  
    let itemData = this.data.shoppingList[target.index]
    let _checked = itemData.checked = !itemData.checked
  
    if (_checked){
      checklist.push(itemData)
      nowPrice += parseInt(itemData.totalPrice*100);
      nowNum += itemData.buyNumber;
    }else{
      checklist = checklist.filter(item => {
        if (item.id == target.id) {
          nowPrice -= parseInt(item.totalPrice*100);
          nowNum -= item.buyNumber;
        }else{
          return item
        }
      })
    }
    this.setData({
      totalNum: nowNum,
      totalPrice: nowPrice/100,
      checkList: checklist
    });
    var _length = wx.getStorageSync('allkeyLength')
    if (checklist.length === _length) {
      this.setData({
        checkAllTrue:true
      })
    }else {
      this.setData({
        checkAllTrue: false
      })
    }
  },
  // checkboxChoose : function (e) {
  //   let target = e.currentTarget.dataset;
  //   let nowPrice = parseInt(this.data.totalPrice*100);
  //   let checklist = this.data.checkList;
  //   let nowNum = this.data.totalNum;
  //   this.data.shoppingList[target.index].checked = !this.data.shoppingList[target.index].checked;
  //   if (this.data.shoppingList[target.index].checked) {
  //     checklist.push(this.data.shoppingList[target.index]);
  //     nowPrice += parseInt(this.data.shoppingList[target.index].totalPrice*100);
  //     nowNum += this.data.shoppingList[target.index].buyNumber;
  //   }else {
  //     for(var i =0 ; i<checklist.length;i++){
  //       if(this.data.shoppingList[target.index].id == checklist[i].id){
  //         nowPrice -= parseInt(checklist[i].totalPrice*100);
  //         nowNum -= checklist[i].buyNumber;
  //         checklist.splice(i,1);
  //       }
  //     }
  //   }
  //   this.setData({
  //     totalNum: nowNum,
  //     totalPrice: nowPrice/100,
  //     checkList: checklist
  //   });
  //   var _length = wx.getStorageSync('allkeyLength')
  //   if (checklist.length === _length) {
  //     this.setData({
  //       checkAllTrue:true
  //     })
  //   }else {
  //     this.setData({
  //       checkAllTrue: false
  //     })
  //   }
  // },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.selectModal = this.selectComponent("#selectModal");
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getShoppingList();
    this.setData({
      checkboxTrue: false,
      checkAllTrue: false,
      totalNum: 0,
      totalPrice: 0,
      checkList: []
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})