// miniprogram/pages/details/details.js
var app = getApp();
var resource = require('../../utils/common/common.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    animationData:'',
    showModalStatus: false,
    b_id:'',
    softwareDetails: '',
    defaultType:0,
    shoppingNum:null,
  },
  toHome: function () {
    wx.switchTab({
      url: '/pages/home/home'
    })
  },
  toShopC: function () {
    wx.switchTab({
      url: '/pages/shoppingCart/shoppingCart'
    })
  },
  showModal: function () {
    this.selectModal.showModal();
  },

  getShoppingNum: function (e) {
    app.getResource('purchaseList', 'get').then((res) => {
      console.log(res)
      this.setData({
        shoppingNum: res.data.length
      })
    })
    
  },
  _hideModalEvent() {
    console.log('监控自定义组件点击事件')
    this.getShoppingNum();
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var self = this;
    app.getResource('homePageList/' + options.id, 'get').then((res) => {
      self.setData({
        b_id: options.id,
        softwareDetails: res.data
      })
    });
    this.getShoppingNum();
  },
  

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.selectModal = this.selectComponent("#selectModal"); 
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})