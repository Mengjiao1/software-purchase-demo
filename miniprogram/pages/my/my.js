// miniprogram/pages/my/my.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    orderList:[],
    month:0,
    day:0,
    year:0,
    _timeSort:'desc',
    _totalSort:'asc',
    activeSort:{
      sortField:'',
      sortWay:''
    },
    page: 1,
    limit: 3,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (app.globalData.userInfo) {
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true,
      });
    }
    
  },
  deleteOrder:function(event){
    app.getResource('personalList/'+event.currentTarget.id, 'delete').then((res)=>{
      app.getResource('personalList', 'get').then((res) => {
        this.setData({
          orderList: res.data
        })
      })
    })
  },
  timeSort : function (e) {
    this.setData({
      page:1
    })
    app.getResource('personalList?' + '_sort=timeStamp&_order=' + e.currentTarget.dataset.sort + '&_page=' + this.data.page + '&_limit=' + this.data.limit,'get').then((response)=>{
      this.setData({
        orderList:response.data,
        _timeSort: this.sort(e.currentTarget.dataset.sort),
        _totalSort:'asc',
        activeSort: {
          sortField: 'timeStamp',
          sortWay: e.currentTarget.dataset.sort
        }
      })
    })
  },
  totalSort : function (e) {
    this.setData({
      page: 1
    })
    app.getResource('personalList?' + '_sort=totalPrice&_order=' + e.currentTarget.dataset.sort + '&_page=' + this.data.page+'&_limit=' + this.data.limit,'get').then((res)=>{
      this.setData({
        orderList:res.data,
        _totalSort: this.sort(e.currentTarget.dataset.sort),
        _timeSort:'desc',
        activeSort:{
          sortField: 'totalPrice',
          sortWay: e.currentTarget.dataset.sort
        }
      })
    });
  },
  sort: function (val) {
    var _val = ''
    switch (val) {
      case 'asc':
        _val = 'desc';
        break;
      case 'desc':
        _val = 'asc';
        break;
      default:
        _val = 'asc';
    }
    return _val
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      page: 1,
      _timeSort: 'desc',
      _totalSort: 'asc',
    })
    app.getResource('personalList?_page=' + this.data.page + '&_limit=' + this.data.limit, 'get').then((res) => {
      this.setData({
        orderList: res.data
      })
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function (e) {
    var _page = this.data.page + 1
    this.setData({
      page: _page
    })
    var joining=''
    if (this.data.activeSort.sortField && this.data.activeSort.sortWay){
      joining = '?_sort=' + this.data.activeSort.sortField + '&_order=' + this.data.activeSort.sortWay + '&_page=' + _page + '&_limit=' + this.data.limit
    }else{
      joining = '?_page=' + _page + '&_limit=' + this.data.limit
    }
    app.getResource('personalList' + joining, 'get').then((res) => {
      this.setData({
        orderList: this.data.orderList.concat(res.data),
      })
    });
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})